#### Capacity

Out of XX business days between MM 18-MM 17 `\~XX%`

List individual PTO entries below

[GitLab workflow timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline)

#### Milestone Board

https://gitlab.com/groups/gitlab-org/-/boards/1318796?&label_name[]=group%3A%3Adatabase

#### Priorities for `%milestone`

#### Stretch Goals

/label ~"group::database" ~"devops::enablement"